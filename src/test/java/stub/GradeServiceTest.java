package stub;

import mock.SecurityCenter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class GradeServiceTest {
    /* 需求描述：
    编写GradeService类的单元测试，单元测试calculateAverageGrades方法
    * */
    private GradeService gradeService;

    @Mock
    GradeSystem gradeSystem;

    @BeforeEach
    void setUp() {
        initMocks(this);
        gradeService = new GradeService(gradeSystem);
    }

    @Test
    void should_Return_90_When_Calculate_StudentAverageGradeAndGradeIs80And90And100() {
        //assertThat(result, is(90.0));
        when(gradeSystem.gradesFor(0)).thenReturn(Arrays.asList(80.0,90.0,100.0));
        double grades = gradeService.calculateAverageGrades(0);
        assertEquals(grades, 90.0);
    }
}