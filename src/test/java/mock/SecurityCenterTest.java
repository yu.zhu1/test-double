package mock;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;


class SecurityCenterTest {
    /* 需求描述：
    编写SecurityCenter类的单元测试，单元测试switchOn方法，不依赖于DoorPanel的close的方法实现
    * */
    private SecurityCenter securityCenter;

    @Mock
    DoorPanel doorPanel;


    @BeforeEach
     void setUp() {
        initMocks(this);
        securityCenter = new SecurityCenter(doorPanel);
    }

    @Test
     void shouldVerifyDoorIsClosed() {
        securityCenter.switchOn();
        verify(doorPanel).close();

    }
}
