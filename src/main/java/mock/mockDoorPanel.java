package mock;

public class mockDoorPanel extends DoorPanel{
    private boolean doorClosed = false;

    public boolean isDoorClosed() {
        return doorClosed;
    }

    @Override
    void close() {
        super.close();
        doorClosed = true;
    }
}
